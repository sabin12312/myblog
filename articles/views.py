from django.shortcuts import render,redirect,get_object_or_404
from django.views import View	
from .forms import ArticleModelForm
from .models import Articles
# Create your views here.

class ArticlePostView(View):
    form_class = ArticleModelForm
    template_name = 'articles/new-articles.html'

    def get(self, request, *args, **kwargs):  
        return render(request, self.template_name,{'form':self.form_class})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>

            obj = get_object_or_404(Articles,pk = self.kwargs['pk'])
            queryset = obj.name_set.all() 
            form = form.save(commit=False)
            form.author = request.user
            form.save()
            return redirect('/articles/articles',pk=self.kwargs.get('pk'))

        return render(request, self.template_name, {'form': form})



class MainPageView(View):
    
    template_name = 'articles/main-category.html'

    def get(self, request, *args, **kwargs):  
        return render(request, self.template_name)




class ArticleDetailView(View):
    
    template_name = 'articles/list-articles.html'
    


    def get(self, request, *args, **kwargs):  
        return render(request, self.template_name)
