from django.urls import path

from articles.views import ArticlePostView,MainPageView,ArticleDetailView
from . import views 

urlpatterns = [
   
    path('newarticle/',ArticlePostView.as_view(),name='newarticle'),
    path('cms/',MainPageView.as_view(),name='main-page'),
    path('articles/',ArticleDetailView.as_view(),name='article')
    #path('<slug:slug>/',PostDetailView.as_view(),name='post_detail')
]