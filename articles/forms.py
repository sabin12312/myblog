from .models import Articles,Category,SubCategory
from django import forms



class ArticleModelForm(forms.ModelForm):
	class Meta:
		model = Articles
		fields=['name','content','parent_category','root_category']