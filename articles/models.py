from django.db import models
from django.contrib.postgres.fields import JSONField
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.
class Articles(models.Model):
	name = models.CharField(max_length=100,unique = True)
	parent_category = models.IntegerField()
	
	content = RichTextUploadingField(blank=True,null=True,config_name = 'special')
	root_category = models.BooleanField()


class Category(models.Model):
	name = models.CharField(max_length=100,unique = True)
	sub_category = JSONField(default=None)



class SubCategory(models.Model):
	sub_name = models.CharField(max_length=100,unique = True)