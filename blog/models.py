
from django.db import models
from django.utils import timezone 
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.

class Post(models.Model):
    likes = models.ManyToManyField(User,related_name='blog_posts')
    title = models.CharField(max_length=100)
    content = RichTextUploadingField(blank=True,null=True,config_name = 'special')
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    def total_likes(self):
        return self.likes.count()
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk' : self.pk})
    

class Comment(models.Model):
    post = models.ForeignKey(Post,on_delete=models.CASCADE,related_name ='comments')
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created_on = models.DateTimeField(default=timezone.now, editable=False)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ('created_on',)

    def __str__(self):
        return 'Comment {} by {}'.format(self.body, self.name)
